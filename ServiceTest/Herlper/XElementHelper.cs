﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace ServiceTest.Herlper
{
    public static class XElementHelper
    {
        public static XElement ToXElement<T>(this object obj)
        {
            using (var memoryStream = new MemoryStream())
            {
                using (TextWriter streamWriter = new StreamWriter(memoryStream))
                {
                    var xmlSerializer = new XmlSerializer(typeof(T));
                    xmlSerializer.Serialize(streamWriter, obj);
                    return XElement.Parse(Encoding.ASCII.GetString(memoryStream.ToArray()));
                }
            }
        }

        public static void AddNameSpace(XNamespace pNameSpace, XElement pXelement)
        {
            foreach (var aux in pXelement.Descendants())
            {
                aux.Name = pNameSpace + aux.Name.LocalName;
            }
        }
    }
}