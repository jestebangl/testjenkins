﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ServiceTest.Models
{
    public class ResponseFactura
    {
        public ErrorStatus ErrorStatus { get; set; }
        public decimal ValorFactura { get; set; }
        public DateTime FechaVencimiento { get; set; }

        public ResponseFactura(ErrorStatus pErrorStatus, decimal pValorFactura, DateTime pFechaVencimiento)
        {
            ErrorStatus = pErrorStatus;
            ValorFactura = pValorFactura;
            FechaVencimiento = pFechaVencimiento;
        }

        public ResponseFactura() { }
    }
}
