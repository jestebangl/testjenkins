﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ServiceTest.Models
{
    [Serializable]
    [XmlRoot(ElementName = "ConsultaPagoResponse", Namespace = "http://generic.uri/WSGenericoRecaudos/")]
    public class ConsultaPagoResponse
    {
        public ResponseFactura ReponseFactura{ get; set; }

        public ConsultaPagoResponse(ResponseFactura pReponseFactura)
        {
            ReponseFactura = pReponseFactura;
        }

        public ConsultaPagoResponse()
        {
        }
    }
}
