﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ServiceTest.Models
{
    [Serializable]
    public class ErrorStatus
    {
        public Int32 Codigo { get; set; }

        public string Mensaje { get; set; }
        public string MensajeAlterno { get; set; }

        public ErrorStatus(int pCodigo, string pMensaje, string pMensajeAlterno)
        {
            Codigo = pCodigo;
            Mensaje = pMensaje;
            MensajeAlterno = pMensajeAlterno;
        }

        public ErrorStatus() { }
    }
}
