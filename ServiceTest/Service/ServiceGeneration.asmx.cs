﻿using ServiceTest.Herlper;
using ServiceTest.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace ServiceTest.Service
{
    /// <summary>
    /// Descripción breve de ServiceGeneration
    /// </summary>
    /// <seealso cref="System.Web.Services.WebService" />
    [WebService]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class ServiceGeneration : System.Web.Services.WebService
    {

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
        public XmlDocument GenerateResponse()
        {
            try
            {
                XNamespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
                XNamespace wsg = "http://generic.uri/WSGenericoRecaudos/";
                XElement root = new XElement(soapenv + "Envelope",
                    new XAttribute(XNamespace.Xmlns + "soapenv", soapenv.NamespaceName),
                    new XAttribute(XNamespace.Xmlns + "wsg", wsg.NamespaceName),
                    new XElement(soapenv + "Header"),
                    new XElement(soapenv + "Body",
                        new XElement(wsg + "ConsultaPagoResponse",
                            new XElement(wsg + "responsefactura",
                                new XElement(wsg + "ErrorStatus",
                                    new XElement(wsg + "Codigo", "3"),
                                    new XElement(wsg + "Mensaje", "Valor Incorrecto"),
                                    new XElement(wsg + "MensajeAlterno", "El valor no corresponde a la factura")
                                ),
                                new XElement(wsg + "ValorFactura", 0),
                                new XElement(wsg + "FechaVencimiento", "2017-08-22T17:17:48.116248-05:00")
                           )
                        )
                    )
                );

                var xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(root.ToString());
                return xmlDocument;
            }
            catch (Exception e)
            {
                throw e;
            }

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Xml)]
        [SoapDocumentMethod(ParameterStyle = SoapParameterStyle.Bare, ResponseElementName = "ConsultaPagoResponse",ResponseNamespace = "http://generic.uri/WSGenericoRecaudos/wsg")]
        [return: XmlElement(ElementName = "ConsultaPagoResponse", Namespace = "http://generic.uri/WSGenericoRecaudos/wsg")]
        public XmlDocument ConsultaPago()
        {
            try
            {
                //NameSpaces
                XNamespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
                XNamespace wsg = "http://generic.uri/WSGenericoRecaudos/";

                //Creación del objeto ConsultaPagoResponse
                ErrorStatus es = new ErrorStatus() { Codigo = 3, Mensaje = "Error", MensajeAlterno = "Error alterno" };
                ResponseFactura rp = new ResponseFactura() { ErrorStatus = es, FechaVencimiento = DateTime.Parse("2017-08-22T17:17:48.116248-05:00"), ValorFactura = 0 };
                ConsultaPagoResponse cpr = new ConsultaPagoResponse() { ReponseFactura = rp };

                // Se cambia el objeto ConsultaPagoResponse a XML
                XElement objetToXML = XElementHelper.ToXElement<ResponseFactura>(rp);
                objetToXML.Add(new XAttribute(XNamespace.Xmlns + "wsg", wsg.NamespaceName));
                objetToXML.Name = wsg + objetToXML.Name.LocalName;

                //Se cambian los namespaces de los nodos dentro del body
                XElementHelper.AddNameSpace(wsg, objetToXML);

                // Se parse el XElement y su etructura un documento XML y se retorna
                var xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(objetToXML.ToString());
                return xmlDocument;
            }
            catch (Exception e)
            {
                throw e;
            }

        }



    }
}
